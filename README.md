**Big Neon Android POC**

**Overview**
*  This Big Neon app will pull data from the events API and display a list of upcoming events to the user.
*  Clicking on an event will enable a user to view more details about the event and venue


**Quick start**

Cloning
*  Clone the repo using `git clone https://gitlab.com/ryan.nair01/bigneon.git`

What you will require - versions as compiled with
*  Android Studio - 3+
*  Java - 8+
*  Android SDK - api 28

Terminal - compiling from terminal will requrie
*  Java home environment variable configuration
*  Android home environemtnvariable configuration

Compiling

*  `./gradlew clean assembledebug`
*  This will generate an APK in the directory app/build/outputs/apk/debug/app-debug.apk

Installing on device

*  Ensure a device is connected - emulator / physical device. You can validate this by opening logcat in Android studio and ensuring your device is visible in the device drop down
*  `./gradlew clean installDebug`
*  Alternatively, open the project in Android Studio, ensure the build varient is debug, and click the Run button and select the connected device

