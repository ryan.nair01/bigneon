package com.android.bigneon.explore.util

import org.junit.Assert.*
import org.junit.Test

class TimeConverterTest {

    @Test
    fun testThatTheGivenTimeZoneConvertDateTimeAccordingly() {
        val actual = TimeConverter.convertToTimeZone(UTC_TIME, TIME_ZONE)
        assertEquals(EXPECTED_TIME_CONVERSION, actual)
    }

    private companion object {
        private const val UTC_TIME = "2019-02-02T04:30:00"
        private const val TIME_ZONE = "America/Los_Angeles"
        private const val EXPECTED_TIME_CONVERSION = "2019-02-01T08:30:00"
    }
}