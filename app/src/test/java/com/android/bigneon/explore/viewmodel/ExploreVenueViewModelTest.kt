package com.android.bigneon.explore.viewmodel

import com.android.bigneon.explore.service.ExploreVenuesService
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class ExploreVenueViewModelTest {

    @Mock
    private lateinit var mockTaskFactory: ExploreVenuesService.Factory
    @Mock
    private lateinit var mockTask: ExploreVenuesService.Task

    private lateinit var viewModel: ExploreVenueViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = ExploreVenueViewModel(mockTaskFactory)
    }

    @Test
    fun testThatInitTheViewFiresOfTask() {
        `when`(mockTaskFactory.createTask()).thenReturn(mockTask)
        viewModel.initView()
        verify(mockTask).execute()

    }
}