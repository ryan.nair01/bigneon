package com.android.bigneon.explore.repo

import com.android.bigneon.explore.cache.ExploreVenueCache
import com.android.bigneon.explore.model.Paging
import com.android.bigneon.explore.model.Tags
import com.android.bigneon.explore.model.VenueDTO
import com.android.bigneon.explore.service.ExploreVenuesService
import org.junit.Assert.assertEquals
import org.junit.Assert.assertSame
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.inOrder
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.util.concurrent.locks.Lock

class ExploreVenueRepoTest {

    @Mock
    private lateinit var mockService: ExploreVenuesService
    @Mock
    private lateinit var mockLock: Lock
    @Mock
    private lateinit var mockCache: ExploreVenueCache

    private lateinit var repo: ExploreVenueRepo

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repo = ExploreVenueRepo(mockService, mockLock, mockCache)
    }

    @Test
    fun testThatRepoReturnsCachedDataIfAvailable() {
        `when`(mockCache.get()).thenReturn(VALID_RESPONSE)
        val response = repo.getVenueList()

        val inOrder = inOrder(mockCache, mockLock)
        inOrder.verify(mockLock).lock()
        inOrder.verify(mockCache).get()
        inOrder.verify(mockLock).unlock()

        assertSame(VALID_RESPONSE, response)
    }

    @Test
    fun testThatServiceIsCalledAndCachedIfCacheDataNotAvailable() {
        `when`(mockCache.get()).thenReturn(INVALID_RESPONSE)
        `when`(mockService.getVenueList()).thenReturn(VALID_RESPONSE)

        val response = repo.getVenueList()

        val inOrder = inOrder(mockCache, mockLock, mockService)
        inOrder.verify(mockLock).lock()
        inOrder.verify(mockCache).get()
        inOrder.verify(mockService).getVenueList()
        inOrder.verify(mockCache).set(VALID_RESPONSE)
        inOrder.verify(mockLock).unlock()

        assertEquals(VALID_RESPONSE, response)
    }

    @Test
    fun testThatIfServiceReturnsNonOKResponseCodeDataIsNotCachedAndEmptyResponseObjectReturned() {
        `when`(mockCache.get()).thenReturn(INVALID_RESPONSE)
        `when`(mockService.getVenueList()).thenReturn(VALID_RESPONSE_INVALID_CODE)

        val response = repo.getVenueList()

        val inOrder = inOrder(mockCache, mockLock, mockService)
        inOrder.verify(mockLock).lock()
        inOrder.verify(mockCache).get()
        inOrder.verify(mockService).getVenueList()
        inOrder.verify(mockLock).unlock()

        assertEquals(ResponseEntity<VenueDTO>(HttpStatus.NOT_FOUND).toString(), response.toString())
    }

    private companion object {
        private val INVALID_RESPONSE = ResponseEntity<VenueDTO>(null)
        private val VALID_RESPONSE = ResponseEntity<VenueDTO>(VenueDTO(ArrayList(), Paging("", 0, 0, "", Tags(), 0)), HttpStatus.OK)
        private val VALID_RESPONSE_INVALID_CODE = ResponseEntity<VenueDTO>(VenueDTO(ArrayList(), Paging("", 0, 0, "", Tags(), 0)), HttpStatus.I_AM_A_TEAPOT)
    }
}