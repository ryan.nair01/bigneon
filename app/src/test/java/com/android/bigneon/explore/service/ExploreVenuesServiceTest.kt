package com.android.bigneon.explore.service

import com.android.bigneon.explore.model.Paging
import com.android.bigneon.explore.model.Tags
import com.android.bigneon.explore.model.VenueDTO
import com.android.bigneon.explore.repo.ExploreVenueRepo
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate

class ExploreVenuesServiceTest {

    @Mock
    private lateinit var mockRestTemplate: RestTemplate
    @Mock
    private lateinit var mockListener: ExploreVenuesService.Listener
    @Mock
    private lateinit var mockRepo: ExploreVenueRepo
    private lateinit var venuesService: ExploreVenuesService
    private lateinit var task: ExploreVenuesService.Task

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        venuesService = ExploreVenuesService(mockRestTemplate)
        task = ExploreVenuesService.Task(mockListener, mockRepo)
    }

    @Test
    fun testThatCorrectHttpCallIsMadeAndTheResultingBodyIsReturned() {
        val expectedBody = VenueDTO(ArrayList(), Paging("", 0, 0, "", Tags(), 0))
        val expectedResponseEntity: ResponseEntity<VenueDTO> = ResponseEntity(expectedBody, HttpStatus.I_AM_A_TEAPOT)

        `when`(mockRestTemplate.exchange("https://bigneon.com/api/events", HttpMethod.GET, null, VenueDTO::class.java)).thenReturn(expectedResponseEntity)

        val actualResponseEntity = venuesService.getVenueList()

        verify(mockRestTemplate).exchange("https://bigneon.com/api/events", HttpMethod.GET, null, VenueDTO::class.java)
        assertSame(expectedResponseEntity, actualResponseEntity)
    }

    @Test
    fun testThatDoInBackgroundCallCorrectServiceMethods() {
        val expected = ResponseEntity<VenueDTO>(VenueDTO(ArrayList(), Paging("", 0, 0, "", Tags(), 0)), HttpStatus.I_AM_A_TEAPOT)
        `when`(mockRepo.getVenueList()).thenReturn(expected)
        val actual = task.doInBackground()

        assertSame(expected, actual)
    }

    @Test
    fun testThatListenerIsNotifiedTheTaskHasStarted() {
        task.onPreExecute()
        verify(mockListener).onTaskStarted()
    }

    @Test
    fun testThatOnPostExecuteWithInvalidResponseCodeCallsError() {
        val expected = ResponseEntity<VenueDTO>(VenueDTO(ArrayList(), Paging("", 0, 0, "", Tags(), 0)), HttpStatus.I_AM_A_TEAPOT)

        task.onPostExecute(expected)

        val inOrder = inOrder(mockListener)
        inOrder.verify(mockListener).onTaskFinished()
        inOrder.verify(mockListener).onTaskError()
    }

    @Test
    fun testThatOnPostExecuteWithInvalidResponseBodyCallsError() {
        val expected = ResponseEntity<VenueDTO>(null, HttpStatus.OK)

        task.onPostExecute(expected)

        val inOrder = inOrder(mockListener)
        inOrder.verify(mockListener).onTaskFinished()
        inOrder.verify(mockListener).onTaskError()
    }

    @Test
    fun testThatOnPostExecuteWithValidResponseCodeAndBodyCallsSuccess() {
        val body = VenueDTO(ArrayList(), Paging("", 0, 0, "", Tags(), 0))
        val expected = ResponseEntity<VenueDTO>(body, HttpStatus.OK)

        task.onPostExecute(expected)

        val inOrder = inOrder(mockListener)
        inOrder.verify(mockListener).onTaskFinished()
        inOrder.verify(mockListener).onTaskSuccess(body)
    }
}