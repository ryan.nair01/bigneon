package com.android.bigneon.details.model

import android.os.Parcel
import android.os.Parcelable

data class ExploreVenueDetailsModel(val name: String?,
                                    val date: String?,
                                    val timeAndLocation: String?,
                                    val artists: String?,
                                    val ageRestriction: String?,
                                    val description: String?,
                                    val image: String?) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(date)
        parcel.writeString(timeAndLocation)
        parcel.writeString(artists)
        parcel.writeString(ageRestriction)
        parcel.writeString(description)
        parcel.writeString(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ExploreVenueDetailsModel> {
        override fun createFromParcel(parcel: Parcel): ExploreVenueDetailsModel {
            return ExploreVenueDetailsModel(parcel)
        }

        override fun newArray(size: Int): Array<ExploreVenueDetailsModel?> {
            return arrayOfNulls(size)
        }
    }

}