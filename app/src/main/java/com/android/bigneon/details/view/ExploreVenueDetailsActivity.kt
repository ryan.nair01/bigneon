package com.android.bigneon.details.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.android.bigneon.R
import com.android.bigneon.details.model.ExploreVenueDetailsModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.explore_venue_details_activity.*

class ExploreVenueDetailsActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.explore_venue_details_activity)

        val data = intent.getParcelableExtra<ExploreVenueDetailsModel>(EXPLORE_DETAILS_EXTRA_KEY)
        explore_details_title.text = data.name
        time_and_location_body.text = data.timeAndLocation
        performing_artist_body.text = data.artists
        age_restriction_body.text = data.ageRestriction
        event_description_body.text = data.description

        Picasso.get()
            .load(data.image)
            .resize(1080, 600)
            .centerCrop()
            .into(explore_details_image)

    }

    companion object {
        const val EXPLORE_DETAILS_EXTRA_KEY = "explore_details_extra_key"
    }
}