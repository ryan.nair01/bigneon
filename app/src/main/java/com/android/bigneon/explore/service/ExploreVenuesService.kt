package com.android.bigneon.explore.service

import android.os.AsyncTask
import com.android.bigneon.explore.model.VenueDTO
import com.android.bigneon.explore.repo.ExploreVenueRepo
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.json.GsonHttpMessageConverter
import org.springframework.web.client.RestTemplate

class ExploreVenuesService(private val restTemplate: RestTemplate = RestTemplate()) {

    fun getVenueList(): ResponseEntity<VenueDTO> {
        restTemplate.messageConverters.add(GsonHttpMessageConverter())
        val response: ResponseEntity<VenueDTO> = restTemplate.exchange(ENDPOINT, HttpMethod.GET, null, VenueDTO::class.java)
        return response
    }

    class Task(private val listener: Listener,
               private val repo: ExploreVenueRepo = ExploreVenueRepo()
    ): AsyncTask<Void, Void, ResponseEntity<VenueDTO>>() {

        public override fun doInBackground(vararg p0: Void?): ResponseEntity<VenueDTO> {
            return repo.getVenueList()
        }

        public override fun onPreExecute() {
            listener.onTaskStarted()
        }

        public override fun onPostExecute(result: ResponseEntity<VenueDTO>) {
            listener.onTaskFinished()
            if (result.statusCode == HttpStatus.OK && result.body != null) {
                listener.onTaskSuccess(result.body)
            } else {
                listener.onTaskError()
            }
        }
    }

    class Factory(private val listener: Listener) {
        fun createTask(): Task {
            return Task(listener)
        }
    }

    interface Listener {

        fun onTaskStarted()

        fun onTaskFinished()

        fun onTaskSuccess(result: VenueDTO)

        fun onTaskError()

    }

    private companion object {
        private const val ENDPOINT = "https://bigneon.com/api/events"
    }


}