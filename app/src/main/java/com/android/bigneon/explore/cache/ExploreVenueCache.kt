package com.android.bigneon.explore.cache

import com.android.bigneon.explore.model.VenueDTO
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

object ExploreVenueCache {

    private var cacheData: ResponseEntity<VenueDTO> = ResponseEntity(HttpStatus.NOT_FOUND)

    fun set(data: ResponseEntity<VenueDTO>) {
        cacheData = data
    }

    fun get(): ResponseEntity<VenueDTO> {
        return cacheData
    }

}