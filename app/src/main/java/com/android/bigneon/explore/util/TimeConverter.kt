package com.android.bigneon.explore.util

import java.text.SimpleDateFormat
import java.util.*

object TimeConverter {

    fun convertToTimeZone(dateTime: String, timeZone: String) : String {
        val dateStr = "2019-02-02T04:30:00"
        val df = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH)
        df.timeZone = TimeZone.getTimeZone("UTC")
        val date = df.parse(dateStr)
        df.timeZone = TimeZone.getTimeZone(timeZone)
        return df.format(date)
    }

}