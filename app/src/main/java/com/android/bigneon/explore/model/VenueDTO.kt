package com.android.bigneon.explore.model

data class VenueDTO(
    val `data`: List<Data>,
    val paging: Paging
)

data class Paging(
    val dir: String,
    val limit: Int,
    val page: Int,
    val sort: String,
    val tags: Tags,
    val total: Int
)

class Tags(
)

data class Data(
    val additional_info: String,
    val age_limit: Int,
    val cancelled_at: String,
    val created_at: String,
    val door_time: String,
    val event_start: String,
    val external_url: String,
    val id: String,
    val is_external: Boolean,
    val localized_times: LocalizedTimes,
    val max_ticket_price: Int,
    val min_ticket_price: Int,
    val name: String,
    val organization_id: String,
    val promo_image_url: String,
    val publish_date: String,
    val status: String,
    val top_line_info: String,
    val tracking_keys: TrackingKeys,
    val user_is_interested: Boolean,
    val venue: Venue,
    val venue_id: String
)

data class TrackingKeys(
    val facebook_pixel_key: String,
    val google_ga_key: String
)

data class LocalizedTimes(
    val door_time: String,
    val event_end: String,
    val event_start: String
)

data class Venue(
    val address: String,
    val city: String,
    val country: String,
    val created_at: String,
    val google_place_id: String,
    val id: String,
    val is_private: Boolean,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val organization_id: String,
    val phone: String,
    val postal_code: String,
    val promo_image_url: String,
    val region_id: String,
    val state: String,
    val timezone: String,
    val updated_at: String
)