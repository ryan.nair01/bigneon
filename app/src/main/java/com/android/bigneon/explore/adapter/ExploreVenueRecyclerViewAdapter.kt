package com.android.bigneon.explore.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.android.bigneon.R
import com.android.bigneon.details.model.ExploreVenueDetailsModel
import com.android.bigneon.details.view.ExploreVenueDetailsActivity
import com.android.bigneon.explore.model.Data
import com.android.bigneon.explore.model.VenueDTO
import com.squareup.picasso.Picasso

class ExploreVenueRecyclerViewAdapter(private val venueList: VenueDTO) :
    RecyclerView.Adapter<ExploreVenueRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val context = viewGroup.context
        val inflater = LayoutInflater.from(context)
        val itemView = inflater.inflate(R.layout.big_neon_explore_item_view, viewGroup, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val listItem: Data = venueList.data[position]
        val imageView: ImageView = viewHolder.image
        val heading: TextView = viewHolder.heading
        val dateTime: TextView = viewHolder.dateTime
        val container: View = viewHolder.container

        Picasso.get()
            .load(listItem.promo_image_url)
            .resize(1000, 400)
            .centerCrop()
            .into(imageView)

        heading.text = listItem.name
        dateTime.text = listItem.publish_date
        container.setOnClickListener {

            val context = viewHolder.container.context
            val intent = Intent(context, ExploreVenueDetailsActivity::class.java)
            val model = Transform.toExploreDetailsModel(venueList.data[position])

            intent.putExtra(ExploreVenueDetailsActivity.EXPLORE_DETAILS_EXTRA_KEY, model)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return venueList.data.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var container: View = view.findViewById(R.id.exploreItemContainer)
        var image: ImageView = view.findViewById(R.id.exploreItemImage)
        var heading: TextView = view.findViewById(R.id.exploreItemHeading)
        var dateTime: TextView = view.findViewById(R.id.exploreItemDateTime)

    }

    private object Transform {

        fun toExploreDetailsModel(data: Data) : ExploreVenueDetailsModel {

            val venue = data.venue
            val timeAndLocation = venue.name + "\n" + venue.address + ", " + venue.city + ", " + venue.state + ", " + venue.country

            return ExploreVenueDetailsModel(data.name,
                data.door_time,
                timeAndLocation,
                data.name,
                data.age_limit.toString(),
                data.additional_info,
                data.promo_image_url)
        }
    }

}