package com.android.bigneon.explore.repo

import com.android.bigneon.explore.cache.ExploreVenueCache
import com.android.bigneon.explore.model.VenueDTO
import com.android.bigneon.explore.service.ExploreVenuesService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

class ExploreVenueRepo(private var service: ExploreVenuesService = ExploreVenuesService(),
                       private var lock: Lock = ReentrantLock(),
                       private var cache: ExploreVenueCache = ExploreVenueCache) {

    fun getVenueList() : ResponseEntity<VenueDTO> {
        lock.lock()
        try {
            val cachedResponse = cache.get()
            return if (cachedResponse.body != null) {
                cachedResponse
            } else {
                val serviceResult = service.getVenueList()
                if (serviceResult.statusCode == HttpStatus.OK && serviceResult.body != null) {
                    cache.set(serviceResult)
                    serviceResult
                } else {
                    ResponseEntity(HttpStatus.NOT_FOUND)
                }
            }
        } finally {
            lock.unlock()
        }
    }


}