package com.android.bigneon.explore.view

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.bigneon.R
import com.android.bigneon.explore.adapter.ExploreVenueRecyclerViewAdapter
import com.android.bigneon.explore.model.VenueDTO
import com.android.bigneon.explore.service.ExploreVenuesService
import com.android.bigneon.explore.viewmodel.ExploreVenueViewModel
import kotlinx.android.synthetic.main.explore_venue_fragment.*

class ExploreVenueFragment : Fragment(), ExploreVenuesService.Listener {

    private lateinit var viewModel: ExploreVenueViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.explore_venue_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ExploreVenueViewModel(ExploreVenuesService.Factory(this))
        viewModel.initView()
    }

    override fun onTaskStarted() {
        progress_loader.visibility = View.VISIBLE
    }

    override fun onTaskFinished() {
        progress_loader.visibility = View.GONE
    }

    override fun onTaskSuccess(result: VenueDTO) {
        val adapter = ExploreVenueRecyclerViewAdapter(result)

        venueRecyclerView?.let {
            it.adapter = adapter
            it.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    override fun onTaskError() {
        view?.let {
            Snackbar.make(it, "Something went wrong", Snackbar.LENGTH_LONG)
        }
    }

    companion object {
        @JvmStatic
        fun getInstance(): Fragment {
            return ExploreVenueFragment()
        }
    }

}