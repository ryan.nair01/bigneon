package com.android.bigneon.explore.viewmodel

import com.android.bigneon.explore.service.ExploreVenuesService

class ExploreVenueViewModel(private val taskFactory: ExploreVenuesService.Factory) {

    fun initView() {
        taskFactory.createTask().execute()
    }


}