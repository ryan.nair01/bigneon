package com.android.bigneon

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.android.bigneon.explore.view.ExploreVenueFragment
import kotlinx.android.synthetic.main.big_neon_main_activity.*

class BigNeonMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.big_neon_main_activity)

        bottom_nav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        displayBottomNameFragment(ExploreVenueFragment.getInstance())
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    displayBottomNameFragment(ExploreVenueFragment.getInstance())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_dashboard -> {
                    //todo implementation out of current scope
                    return@OnNavigationItemSelectedListener false
                }
                R.id.navigation_notifications -> {
                    //todo implementation out of current scope
                    return@OnNavigationItemSelectedListener false
                }
            }
            false
        }

    private fun displayBottomNameFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.bottom_nav_content_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

}
